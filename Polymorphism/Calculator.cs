﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    class Calculator
    {
        private ILogger _logger;

        public Calculator(ILogger logger)
        {
            this._logger = logger;
        }

        public void Write(string message)
        {
            this._logger.Write(message);
        }
    };
}
