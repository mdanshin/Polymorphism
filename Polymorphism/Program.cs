﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    class Program
    {
        static void Main(string[] args)
        {
            ILogger consoleLogger = new ConsoleLogger();
            ILogger fileLogger = new FileLogger();

            Calculator calc1 = new Calculator(consoleLogger);
            calc1.Write("Hello world!\n");

            Calculator calc2 = new Calculator(fileLogger);
            calc2.Write("Hello world!\n");
        }
    }
}
