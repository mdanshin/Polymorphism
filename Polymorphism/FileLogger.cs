﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    class FileLogger : ILogger
    {
        public void Write(string message)
        {
            Console.WriteLine("File Logger");
            Console.WriteLine(message);
        }
    };
}
